const express = require("express");

// app - server 
const app = express();

const port = 3000;

//Middlewares - software  that provided common services and capabilities to application outside of what's offered by the operating system

// allows your app to read JSON data
app.use(express.json());

//allow your app to read data from any other forms
app.use(express.urlencoded({extended: true}));

app.listen(port, () => console.log(`Server running at ${port}`));

// [SECTION] ROUTES
// GET Method
app.get("/greet", (request, response) => {
		response.send("Hello from the /greet endpoint!");
});

// POST Method
app.post("/hello", (request, response) => {
		response.send(`Hello there, ${request.body.firstname} ${request.body.lastname}!`)
});

// Simple registration form

let users = [];

app.post("/signup", (request, response) => {

	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	}else{
		response.send("Please input BOTH username and password.");
	}

})

app.patch("/change-password", (request, response) => {
	let message;
	for(let i=0 ; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;

			break;
			}
				else {
					message = "User does not exist."
				}
			}
		


	
	response.send(message);

});

/*ACTIVITY*/

/*Get /home*/
app.get("/home", (request, response) => {
		response.send("Welcome to the home Page");
});

/*Get /users*/
app.get("/users", (request, response) => {
		response.send(users);
});

/*Delete /delete-user*/
app.delete('/delete-user', (request, response) => {
   response.send(`User ron123 has been deleted.`);
});
	
